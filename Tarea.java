/* INTRODUCIR DATOS Y MOSTRAR DATOS POR CONSOLA Y PEDIR DATOS 
NUEVAMENTE CUANDO EXISTA UN ERROR O EXEPCIONES */ 


import java.util.InputMismatchException;
import java.util.Scanner;

public class Tarea {
    public static void main(String[] ar) {
        Scanner in = new Scanner(System.in);
        int num;
        boolean verdadero;
        do {
            try {
                verdadero= false;
                System.out.print("DIGITE UN NUMERO ENTERO:");
                num = in.nextInt();
                
                System.out.print("EL NUMERO INGRESADO ES "+ num);
                
            } catch (InputMismatchException e) {
                System.out.println("POR FAVOR INGRESE UN NUNMERO ENTERO ");
                in.next();
                verdadero = true;
            }
        } while (verdadero);
    }
}

